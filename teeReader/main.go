/*
io.TeeReader - применяется, когда тело HTTP-запроса нужно прочитать дважды.
Концепция заключается в копирование содержимого одного ридера в новый
Хорошо подходит для логирования запросов с помощью middleware перед их обработкой
*/
package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi/v5"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

// User - структура, которая будет передана в теле запроса
type User struct {
	Name string `json:"name"`
}

func main() {
	rout := router()

	log.Fatal(http.ListenAndServe(":8080", rout))
}

// router - создание нов го роутера gochi
func router() http.Handler {

	r := chi.NewRouter()

	r.Use(middleware) // логирование запроса с помощью io.TeeReader и дальнейшая отправка тела запроса по его ручке
	r.Post("/hello", printName)

	return r
}

func middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// создание буфера для логирования данных запроса(для дальнейшего сбора тела в исходное состояние)
		var buf strings.Builder

		// использование io.TeeReader для помещения данных запроса в буфер (buf) и для дублирования данных(teeReader)
		teeReader := io.TeeReader(r.Body, &buf)

		// чтение данных из запроса и их логирование
		body, err := io.ReadAll(teeReader)
		if err != nil {
			http.Error(w, "Ошибка при чтении данных из тела запроса", http.StatusInternalServerError)
		}
		log.Println("Тело запроса содержит следующие данные: ", string(body))

		// запись данных обратно в тело запроса для последующей обработки
		r.Body = ioutil.NopCloser(strings.NewReader(buf.String()))

		// продолжение обработки запроса
		next.ServeHTTP(w, r)
	})
}

func printName(w http.ResponseWriter, r *http.Request) {

	var user User

	err := json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		panic(err)
	}

	fmt.Println(user)
}
